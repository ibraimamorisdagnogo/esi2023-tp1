import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: '',
    redirectTo: 'mets',
    pathMatch: 'full',
  },
  {
    path: 'mets',
    loadChildren: () =>
      import('./pages/mets/mets.module').then((m) => m.MetsPageModule),
  },
  {
    path: 'detail-met',
    loadChildren: () =>
      import('./pages/detail-met/detail-met.module').then(
        (m) => m.DetailMetPageModule
      ),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
