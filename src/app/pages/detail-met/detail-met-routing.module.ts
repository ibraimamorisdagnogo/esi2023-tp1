import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailMetPage } from './detail-met.page';

const routes: Routes = [
  {
    path: '',
    component: DetailMetPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailMetPageRoutingModule {}
