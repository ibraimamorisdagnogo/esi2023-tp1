import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailMetPageRoutingModule } from './detail-met-routing.module';

import { DetailMetPage } from './detail-met.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailMetPageRoutingModule
  ],
  declarations: [DetailMetPage]
})
export class DetailMetPageModule {}
