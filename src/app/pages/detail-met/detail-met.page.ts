import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-met',
  templateUrl: './detail-met.page.html',
  styleUrls: ['./detail-met.page.scss'],
})
export class DetailMetPage implements OnInit {
  objet: any;
  constructor(private activatedRoute: ActivatedRoute) {}

  getObject() {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.objet = JSON.parse(params['objetJSON']);
      if (params && params['objetJSON']) {
        this.objet = JSON.parse(params['objetJSON']);
      }
    });
  }

  ngOnInit() {
    this.getObject();
  }
}
