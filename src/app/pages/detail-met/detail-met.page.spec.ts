import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DetailMetPage } from './detail-met.page';

describe('DetailMetPage', () => {
  let component: DetailMetPage;
  let fixture: ComponentFixture<DetailMetPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(DetailMetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
