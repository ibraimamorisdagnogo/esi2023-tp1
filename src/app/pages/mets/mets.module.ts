import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MetsPageRoutingModule } from './mets-routing.module';

import { MetsPage } from './mets.page';
import { PipesModule } from 'src/app/pipes/pipes.module';

@NgModule({
  imports: [
    CommonModule,
    PipesModule,
    FormsModule,
    IonicModule,
    MetsPageRoutingModule,
  ],
  declarations: [MetsPage],
})
export class MetsPageModule {}
