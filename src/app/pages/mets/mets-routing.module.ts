import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MetsPage } from './mets.page';

const routes: Routes = [
  {
    path: '',
    component: MetsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MetsPageRoutingModule {}
