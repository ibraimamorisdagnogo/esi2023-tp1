import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MetsPage } from './mets.page';

describe('MetsPage', () => {
  let component: MetsPage;
  let fixture: ComponentFixture<MetsPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(MetsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
