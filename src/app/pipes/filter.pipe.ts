import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  transform(table: any[], text: string): any[] {
    if (text === '') {
      return table;
    }
    text = text.toLowerCase();
    return table.filter((item) => {
      return (
        item.Nom.toLowerCase().includes(text) ||
        item.Origine.toLowerCase().includes(text)
      );
    });
  }
}
